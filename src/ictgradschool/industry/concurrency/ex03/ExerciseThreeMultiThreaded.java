package ictgradschool.industry.concurrency.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @SuppressWarnings("Duplicates")
    @Override
    protected double estimatePI(long numSamples) throws InterruptedException {
        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        List<Thread> threads = new ArrayList<>();
        int numThreads=10;

        class StoredVal{
            public long getNumInC() {
                return numInC;
            }

            public synchronized void setNumInC(long numInC) {
                this.numInC += numInC;
            }
            long numInC=0;
        }

        StoredVal store = new StoredVal();


        Runnable subUnit = new Runnable() {
            @Override
            public void run() {
                ThreadLocalRandom tlr = ThreadLocalRandom.current();

                long numInsideCircle = 0;

                for (long i = 0; i < (numSamples/numThreads); i++) {


                    double x = tlr.nextDouble();
                    double y = tlr.nextDouble();

//                    if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                    if ((x*x) + (y*y) < 1.0) {
                        numInsideCircle++;
                    }

                }
                store.setNumInC(numInsideCircle);
            }
        };


        for (int i = 0; i < numThreads; i++) {
            threads.add(new Thread(subUnit));
        }

//start the thread list.
        for (Thread thread:threads
        ) {
            thread.start();
        }
//wait for them to stop
        for (Thread thread:threads
        ) {
            thread.join();
        }



//        long numInsideCircle = 0;
//
//        for (long i = 0; i < numSamples; i++) {
//
//
//            double x = tlr.nextDouble();
//            double y = tlr.nextDouble();
//
//            if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
//                numInsideCircle++;
//            }
//
//        }
//
        double estimatedPi = 4.0 * (double) store.getNumInC() / (double) (numSamples/numThreads * numThreads);
        return  estimatedPi;
    }

    /** Program entry point. */
    public static void main(String[] args) throws InterruptedException {
        new ExerciseThreeMultiThreaded().start();
    }
}
