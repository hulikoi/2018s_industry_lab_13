package ictgradschool.industry.concurrency.ex04;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class ConcurrentBankingApp {
    @SuppressWarnings("Duplicates")
    public static void main(String[] args){
        final BlockingQueue<Transaction> que = new ArrayBlockingQueue<>(10);
        // Acquire Transactions to process.
        final List<Transaction> transactions = TransactionGenerator.readDataFile();
        BankAccount account = new BankAccount();
        List<Thread> threads = new ArrayList<>();

        //create one producer and two consumer threads.
        //producer retrieves a list of transactions from the transactiongenerator.
        //and adds to blockingqueue object, this object initiallised with capacity
        //of 10 transactions.
        //the two consumers should retrieve transactions from the que and apply to
        // the bankaccount object.
        //

        Thread consumer_1 = new Thread(new Consumer(account,que));
        Thread consumer_2 = new Thread(new Consumer(account,que));
        //New thread to add the transactions to the queue, this will be used by
        // the consumer threads to process the transactions.
        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {

                    for (Transaction transaction : transactions) {
                        try {
                            que.put(transaction);
                        } catch (InterruptedException e) {

                            e.printStackTrace();
                        }
                    }
                }

        });

//Start the producer thread so the que gets filled
        producer.start();
//add the consumer threads to a list so they can be manipulated as a unit
        threads.add(consumer_1);
        threads.add(consumer_2);
//start consumer threads
        for (Thread thread:threads) {
            thread.start();
        }

//signal producer is fin. when the code continues to
// execute that means the producer has completed the
// task and the consumers can be stopped.
        try {
            producer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


//stop the consumers
        for (Thread thread:threads) {
            thread.interrupt();
        }

//wait for the threads to stop
        for (Thread thread:threads
             ) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        System.out.println("Final balance: " + account.getFormattedBalance());

    }

}
