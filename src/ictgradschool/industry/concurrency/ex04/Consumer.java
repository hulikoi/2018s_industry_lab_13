package ictgradschool.industry.concurrency.ex04;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{
    private final BankAccount account;
    private final BlockingQueue<Transaction> que;

    //construct with bankaccount and blocking que
    public Consumer(BankAccount account,BlockingQueue que){
        this.account=account;
        this.que=que;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void run() {
        // check if there are still items in the que to be processed,
        // or if the thread has been interupted. If so, take a
        // transaction from the que and process it.
            try {
                while(!que.isEmpty()||!Thread.currentThread().isInterrupted()) {
                    Transaction transaction = que.take();

                    switch (transaction._type) {
                        case Deposit:
                            account.deposit(transaction._amountInCents);
                            break;
                        case Withdraw:
                            account.withdraw(transaction._amountInCents);
                            break;
                    }
                }
            } catch (InterruptedException e) {
                //catch the interuption, this is not a "real" error
                // we intended for the que to be interupted but it
                // is still an exception. make sure the interupted
                // flag on the thread is set. but no other action is required.
                Thread.currentThread().interrupt();
//                System.out.println("interupted");
//                e.printStackTrace();
            }
        }
    }


