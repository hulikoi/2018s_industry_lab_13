package ictgradschool.industry.concurrency.ex05;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PrimeFactorsApp {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Long n = Long.valueOf(0);
        List<Thread> threadList = new ArrayList<>();

        String userIn = "";

        while (userIn.length() == 0) {
            System.out.print("Please enter a number: ");
            userIn = sc.nextLine();
            try {
                n = Long.parseLong(userIn);
            } catch (NumberFormatException e) {

            }
        }

//create task and add to thread
        PrimeFactorsTask task = new PrimeFactorsTask(n);
        Thread taskThread = new Thread(task);

        Thread halt = new Thread(new Runnable() {
            @Override
            public void run() {
                if (sc.hasNext()) {
                    taskThread.interrupt();
                }
            }
        });


//start threads
        halt.start();
        taskThread.start();

//wait for main task to finish
        try {
            taskThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//interrupt halt thread
        halt.interrupt();


//get the list of primes from our task object

        List<Long> primes = null;
        try {
            primes = task.getPrimeFactors();
        } catch (IllegalStateException e) {
            System.exit(0);
        }
//loop through the list of primes and print to console
        for (Long prime : primes
        ) {
            System.out.println(prime);
        }
        System.exit(0);
    }
}
